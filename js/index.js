    $(function(){
      $("[data-toggle='tooltip']").tooltip();
      $("[data-toggle='popover']").popover();
      $('.carousel').carousel({
        interval: 5000
      });
      $('#exampleModal').click('show.bs.modal', function (e){
        console.log('El modal se esta mostrando');
       $('mensaje').removeClass('btn btn-success');
       $('mensaje').addClass('btn btn-primary');
       $('mensaje').prop('disabled', true);
      });
      $('#exampleModal').on('shown.bs.modal', function (e){
        console.log('El modal se mostro');
      });
      $('#exampleModal').on('hide.bs.modal', function (e){
        console.log('El modal se esta escondiendo');
      });
      $('#exampleModal').on('hidden.bs.modal', function (e){
        console.log('El modal se escondio');
      });
    });
